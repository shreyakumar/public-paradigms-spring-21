package main

import(
    "fmt"
    "bufio"
    "net/http"
)

func main(){
    //make network request
    fmt.Println("starting go code...")
    urlString := "https://api.agify.io/?name=" + "Bagheera"

    resp, err := http.Get(urlString) // makes the request

    if err != nil {
        fmt.Println("connection error: ", err)
    }
    defer resp.Body.Close() // spl defer - executed at the end of the scope

    // print the response
    fmt.Println("Response status: ", resp.Status)
    scanner := bufio.NewScanner(resp.Body)
    if scanner.Scan(){
        fmt.Println(scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        fmt.Println("scanner error: ", err)
    }

} // end of main
