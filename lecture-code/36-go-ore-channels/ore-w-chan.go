// go example - ore smelting with simple function style - with channels
package main

import (
	"fmt"
	"time"
)

// ore finder
func finder(mine [5]string, oreChannel chan string){
    fmt.Println("Started finder function.")
    for _, item := range mine {
        if item == "ore" {
	    fmt.Println("Found: ", item)
            oreChannel <- item //send item on oreChannel

        }
    }
} // end of finder

// Ore Breaker
func oreBreaker(oreChannel chan string, minedOreChan chan string) {
	fmt.Println("Started oreBreaker function.")
	for i := 0; i < 3; i++ {
		foundOre := <-oreChannel //read from oreChannel
		fmt.Println("Breaking: ", foundOre)
		minedOreChan <- "minedOre" //send to minedOreChan


	}
} // end of ore breaker

// Smelter
func smelter(minedOreChan chan string) {
	fmt.Println("Started smelter function.")
	for i := 0; i < 3; i++ {
		minedOre := <-minedOreChan //read from minedOreChan
		fmt.Println("Smelting: ", minedOre)
	}
} // end of smelter

func main() {

	theMine := [5]string{"rock", "ore", "ore", "rock", "ore"}

	oreChannel := make(chan string)
	minedOreChan := make(chan string)

	go finder(theMine, oreChannel)
	go oreBreaker(oreChannel, minedOreChan)
	go smelter(minedOreChan)

	<-time.After(time.Second * 1)
} // end of main

// output of this can be something like this:
// Entered main
// Started smelter function.
// Started oreBreaker function.
// Started finder function.
// Found:  ore
// Found:  ore
// Breaking:  ore
// Breaking:  ore
// Found:  ore
// Smelting:  minedOre
// Smelting:  minedOre
// Breaking:  ore
// Smelting:  minedOre

